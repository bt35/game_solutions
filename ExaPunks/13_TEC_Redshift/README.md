#### Trash World News Solution

Solution for the Redshift Code.  

Score: Not Optimized and not fast. 

The solution is pure brute force and its big. The gif is too big to fit here I suspect. 

Here is how I made my gif smaller. Original size was 13MB, and went through all possible 3 digit combos. Smaller file was 7MB. 

```
ffmpeg -t 2 -i ExaSolution.gif -r "15" Solution.gif

```


![](Solution.gif)

#### How's it work?

This level was fun except for one aspect. Copying one digit at a time was a pain, and I had to google how to do that. I am used to high level languages and being able to for loop through all possible numbers without neeing to push each digit to a register. 

I'll share how I worked up to this solution. 

```
#include <iostream>
using namespace std;

/* 
Constraints:
3 digits. 
Each digit must be its own loop.
Each loop must iterate through all possible digit for their digit position. 
The function should iterate through all possible combinations of 0-9 for each digit. 
*/

/*
  This may not work in Exapunks, but it might. 
  We'll need to output 0s in the spots of the smaller numbers. 
*/
int conventionalBruteForce(){
      for(int i=000; i<=999; i++){
      if (i < 10) {
          cout<<"Value of variable i is: "<<i<<00<<endl;
      }
      else  if (i > 10 && i > 100){
          cout<<"Value of variable i is: "<<i<<0<<endl;
      }
      else  if (i > 100 && i < 1000){
      cout<<"Value of variable i is: "<<i<<endl;
   
      }
    } 
    return 0;
}

/*
 * We can use files in Exapunks... 
 * Let's try writing each digit posibility to a file. Like so:
 * 9,9,9
 * 9,9,8
 * 9,9,7
 * 
 * I should practice math... cause this is a math problem and I needed to look at stack overflow:
 * https://codereview.stackexchange.com/questions/66823/output-digits-of-a-number   
 */

int pain(){
   int uglyCount = 999;
   
   while (uglyCount > 0) {
       
    // This is excellent in my opinion.
    /* Any new programmer or old programmer should 
       write at least 5 simple math programs a day.
       To get in the habit of solving problems with math
       instead of procedure. 
    */     
    int one = uglyCount / 100 % 10;
    int two =  uglyCount / 10 % 10;
    int three = uglyCount % 10;
    
    
    cout << one <<"," << two <<"," << three << "," <<endl;
    
    uglyCount--;
   }
   // Test conventionalBruteForce
}


int main(){
    
  pain();

    
   // Test conventionalBruteForce
   // conventionalBruteForce();
   return 0;
}
```

Here's how XA works:

```
COPY 999 X      # Copy 999 into X for the counter
LINK 800        # Go to the next host
MAKE            # Make a file to write stuff into 

MARK MAIN       # Main loop. This is just to keep things clean
 JUMP BRUTE     # Jump to the Brute Force Function

JUMP MAIN       # When done with that cycle of Brute go back to main

MARK BRUTE      # Declare brute force function
 SEEK -9999     # Seek to the beginning of the file. 
 TEST X = 0     # Test if X is 0, and if so go to Next, otherwise continue
 TJMP NEXT      

 DIVI X 100 T   # Divide X by 100 and then Mod it by ten to get the digt. 
 MODI T 10 T    # Example, 999, 998, 910 all have the first digit of 9
 COPY T #PASS   # Copy that digit to #PASS
 COPY T F       # Copy that digit to the file just in case it part of the combo.

 DIVI X 10 T    # Divid X by 10 and Mod it by to again to get the 10th place digit. 
 MODI T 10 T    # Example 999,995,991, all have 9 as the 10th place digit. 
 COPY T #PASS   # Copy digit to #PASS
 COPY T F       # Copy to file in case the digit is part of the combo.

 MODI X 10 T    # Only have to mod the 1st digit by 10 to get it. 
 COPY T #PASS   # Copy that digit to #PASS to see
 COPY T F       # Copy that digit to the file in case it is etc. 

 REPL GO        # Make a clone... to check if the Link is open.

 SUBI X 1 X     # Subtrace 1 from the counter. BRUTE FORCE!
JUMP MAIN       # Go back to main, and start the loop over from the next code.

MARK GO        # Declare GO 
 LINK 800      # If the 800 link is present this exa will go, otherwise it will splode. 
 COPY 1 M      # We're in! Now tell the world! IE: copy 1 to global register
 GRAB 199      # Grab 199 to get the RDX code
 COPY F M      # Copy that code to the global register
 JUMP DIE      # Die, leave no trace
JUMP MAIN      # Go back to main. 

MARK NEXT      # Link 800, this is part of process to check if the code has been broken by testing to see if 800 is open
 LINK 800
 
 GRAB 199      # Grab the file
JUMP NEXT      

MARK DIE       # Need to kill everything off and get rid of that file we created
 WIPE
 HALT
```

XB

```
COPY M X		# Listen for 1, to see if we cracked the code. 
MAKE			# Make a file to store the code and registration in
COPY M F        # Copy the registration code into the file
DROP            # Drop the file
TEST X = 1      # Test if X = 1
TJMP KILL       # If X = 1 jump to kill.
MARK KILL       # Kill
 LINK 800       # Jump to 800, the next host
 KILL           # kill the other exa in there
 REPL GOBACK    # Clone self and GOBACK

 GRAB 400       # Grab the file 400
 COPY F M       # Copy each digit of the successful code into the global reg
 COPY F M       
 COPY F M
 TEST M = 1     # Test if M = 1 
 TJMP DIE       # if M = 1 die 
DROP            # Drop the file

MARK GOBACK     # Mark GOBACK
 LINK -1        # The clone goes back to original host
 GRAB 401       # Grab the original file, the drop box
 COPY F X       # Copy the registration code to X for safe keeping
 SEEK -9999     # Seek to beginning of file
 COPY M F       # Copy each digit of the successful code from M into the file.
 COPY M F
 COPY M F
 COPY X F       # Then copy the registration code back into the file after the login code as per the win conditions
 COPY 1 M       # Send 1 to the M register to trigger kill states
 
 
 DROP           # Drop
 HALT           # Die
 
MARK DIE        # Handle Kill States
WIPE
HALT 
```




