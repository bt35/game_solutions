### My Solution for UC Berkeley Puzzle 

My Solution uses one EXA to send and receive stuff from the HOME host, and the other EXA to move to the required **TAPE HOST**

![](Solution.gif)

#### How's it Work?

This solution is ugly... and not optimized. But I struggle with anything that isn't a for loop. So as you can see the solution is brute force! There are a total of 7 ATM slots. So I generate 7 **EXA**s using **REPL** and they each get the total cash in the **CASH** register. Then the loop counts down via subtraction from that total, and we write 20 to the **DISP** register every time we want to spit some cash out. 

This could be optimized by having a loop do the **REPL** and that might knock off a lot of the lines of code. I forget but I eithe read or was told by a mentor that repeated lines of code can be removed via a loop usually. 

I'll optimize this at some point and the optimized **EXA** too. 

#### Optimized Version 
The optimized version uses two **EXAS**. One does the **REPL**ing and the other stores the **LINK** counter, and also tracks how many **EXA**s were created. Once the **REPL** count is at 7 the second **EXA** **KILL**s the progenator and then itself. 

![](Solution2.gif)
