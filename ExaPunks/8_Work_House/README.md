### My Solution for Work House 


![](Solution.gif)

#### How's it Work?

This is another ugly solution. **XA** Is used to send EMBER-2's Workhouse username to **XB**. Then **XB** uses a loop to go through the file to find EMBER-2's file number. Then it sends that file number to **XC**. **XC** gets the sum of EMBER-2s totals from her file. Then **XC**'s clone **XC-0** divides the sum of EMBER-2's file to get an idea how many times 75 will go into the sum and it sends that result to **XC**. Then it does sum modulus 75 to see what the remainder would be, and sends it to **XB**. Then **XC** overwrites EMBER-2's file with 75 for the total of the results of the division step. Then for the last action it will copy the results of the modulus option from **XB** to the file, which evens out the number. 

I think this solution is ugly and I suspect my explanation above has the **EXAs** out of order too. I find the replication method to be very confusing. 

#### Python Script that I used to test the first iteration of the puzzle. 
```

mod = 7441 % 75 
duh = 0 
for i in range(99):
	duh = duh + 75
	print (duh)

duh = duh + mod # 16 is the modulus of 75 % 7441. We use the remainder to add to the end of the file to make an even entry. 
```
