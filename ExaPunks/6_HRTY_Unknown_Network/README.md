### Solution for Unknown Network

My solution to this puzzle uses 4 EXAs and is not optimized, nor is it small enough to be scored. Each of the 4 original EXAs can have their code condensed into one EXA, but I did not want to 
debug that tonight. 

![](Solution.gif)

#### How's it work?

Each of the 4 EXAs get the HOME host name and put it in the X register. Then each of the 4 EXAs move to row 3 of the puzzle, that's why there are 4 of them. Once in the third row of the puzzle the EXAs will **REPL A** and make a new EXA that **JUMPS** to **MARK N** which means the original EXA will do **MARK A**. The duplicate EXA will do the work in **MARK N** but both EXAs will do work in **MARK G** once they have moved to the last row. Each of the 4 original EXAs contain code that tells the replicant to move opposite of the original. Once in **MARK G** each EXA will try and kill any other EXA in the same host, hopefully one of those EXAs will kill the EXA holding the file we need. After a successful kill the EXA grab the file and take it home. Note if there is no file present the EXA will die which means a **HALT** command isn't needed. Once the host has gotten home drop the file and halt. 

#### Example XA code
```
HOST X
LINK 800
LINK 801
LINK 800
NOTE REPL 
REPL A
JUMP N
MARK A
 LINK 801
 JUMP G
MARK N 
 LINK 800
 JUMP G
MARK G
 KILL
 GRAB 276
MARK LOOP
 LINK -1
 HOST T
 TEST T = X
 TJMP HOME
JUMP LOOP
MARK HOME
 DROP
HALT
``` 

#### Optimization

This code can be optmized by making only one EXA copy itself and move to the last row of each, and then do the kill functions. 

#### Good Luck and Happy Optimizing. 
