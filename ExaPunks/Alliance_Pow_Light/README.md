#### Trash World News Solution

My solution for Alliance Power and Light. 

Score:
None... The solution was too big. 

![](Solution.gif)

#### How's it work?

This solution is a Naive Path Checker. It is 4 EXAs large and utilizes NOOPs. 

Each EXA searches for the opposite item in a different direction in order to cover all possible path failures due to a power off. The goal is to keep the exas as closely timed as possible to limit the chance for path failure due to a power off. 

The solution was tailored by failing at 16, 29, and 41. 


