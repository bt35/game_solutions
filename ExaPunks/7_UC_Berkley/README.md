### My Solution for UC Berkeley Puzzle 

My Solution uses one EXA to send and receive stuff from the HOME host, and the other EXA to move to the required **TAPE HOST**

![](Solution.gif)

#### How's it Work?

XB does nothing for 4 cycles, so that XA can Grab file 300, Read the Russian word and then drop the file. Then XB grabs the file and reads the **TAPE HOST** name into its **X** register. 

Next the XA begins a loop to see if it has moved to the correct **TAPE HOST**. It checks to see if XB has sent the **TAPE HOST** name to it by checking the **M** register. If the **M** register matches the **HOST** name, XA will **JUMP** to **MARK STOP**. XA then sends a 1 to the **M** register and this lets XB know to exit the first **COPY** loop. XA then grabs the **TAPE FILE** while XB drops the original file, voids the **M** register. XA then reads the **TAPE FILE** up to the Russian word in the **MARK READ** function, and then reads the two numbers after it in the **MARK N**. Then in the **MARK RA** function we seek back to the beginning of the **TAPE FILE**, and then seek to number stored in **X** register. This should put the cursor one position before the first position of the data we need. Then we copy the **T** register, the character count nubmer, to the **X** register. We need to add 1 to the **X** register in order to put the cursor on the correct data start point. Then we move to **MARK RN** and we read while subtracting from **X** until **X** equals 0. Then we exit to **MARK END**, send a 1 over to the **M** register which tells XB to finish. The whole time XA was working in **MARK RN** it was copying the data we needed to **M** and XB was in turn coping **M** to a file it had created in the **HOME** host. So once XA tells XB to stop, XB drops the file and **HALTS**. 

If you all have read The Deacon's guide on reading **TAPE FILES** you will know that the two numbers after a word refer to the data itself. That is to say the first number tells you were to seek in the **TAPE FILE** and the second tells how many characters to read. 



#### XA

```
GRAB 300
SEEK 1
COPY F X 
DROP
MARK TAPE
 LINK 800 
 GRAB 200
MARK READ 
 TEST F = X
 TJMP N
 JUMP READ 
MARK N 
 COPY F X
 COPY F T
 JUMP RA
MARK RA
 SEEK -9999
 SEEK X
 COPY T X
 ADDI X 1 X 
 MARK RN
  SUBI X 1 X
  TEST X = 0
  TJMP END
  COPY F M 
 JUMP RN
MARK END
COPY 1 M
 DROP
 HALT
```

#### XB

```
NOOP
NOOP
NOOP
NOOP
GRAB 300
COPY F X
MARK LOOP
 TEST MRD
 TEST T = 1
 TJMP D
 COPY X M 
 JUMP LOOP
MARK D
 DROP
MAKE
VOID M 
MARK COPY
 COPY M X
 TEST X = 1
 TJMP END
 COPY X F
 JUMP COPY
MARK END 
 DROP
 HALT
```
