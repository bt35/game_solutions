#### Trash World News Solution

Solution for the Digital Projects EXA Punks level. 

Score:
1679/74/56

![](Solution.gif)

#### How's it work?

This solution is commented out using the EXA Language standard comment, **NOTE**. The code is well commented, however there are only 25 characters allowed per line of code in an EXA file. Since the EXA language does not have multiline comments there will be sentences broken up by the word **NOTE**. It is best to ignore the word **NOTE** when reading a comment. 

Here is a quick summary of how the solution works. *XA* stays in LocalHost, sends book numbers to *XB* using the Global Register **M**, it also controls the iteration through the Book List by keeping track of iteration in a separate file, and finally it tells *XB* when to **HALT**. *XA* also copies the book to a new file on LocalHost which *XB* sends over **M**.  *XB* listens for the Book Number and iterates through the hosts based on the first digit of the Book Number, once in the target Host it will send contents back over **M** to *XA*, and it will also listen for the kill signal. 

Each EXA has some unique things that they do in order to make the solution work which are detailed in the comments. I needed to use files as registers since there were not enough registers to store iteration count, or book names. So *XA* uses a file created at the very begining to keep track of how many times it has looped through its main loop, and *XB* uses a file to keep track of the Book Number. 

#### Optimization? 

*XA* and *XB* can probably have their code reworked to be cleaner, but the solution overall is not very elegant. A better solution would be to use similar processes but instead of using one Exa to loop through for each book, **REPL** however many are needed to get each book at the same time. Basically instead of an iterative solution a multi EXA solution. I have a solution partially created for the **REPL** method but since I find the EXA language difficult to keep clean and uncomplicated it will take some time to complete.   
